# UCSB Library History

## Description
Content files for the UCSB Library History (flash video) made for the 50th anniversary of the UCSB Library. Those files were available via https://misc.library.ucsb.edu/history/ until support for flash was discontinued.

